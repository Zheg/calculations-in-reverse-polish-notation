import java.util.*
import kotlin.math.pow

fun main(args: Array<String>) {
    var option: Int? = 4
    while (true) {
        try {
            // print("""
            //     |Choose option:
            //     |1. Convert expression to reverse polish notation.
            //     |2. Evaluate reverse polish notation.
            //     |3. Evaluate expression.
            //     |4. Convert expression to reverse polish notation and evaluate it.
            //     |5. Exit.
            //     |""".trimMargin()
            // )
            // option = readLine()!!.toIntOrNull()
            when (option) {
                null -> print("Wrong input!")

                1 -> {
                    print("Input expression: ")
                    print("Reverse polish notation is: " + toRPN(readLine()!!))
                }

                2 -> {
                    print("Input reverse polish notation: ")
                    print("Result is: " + evaluateRPN(readLine()!!))
                }

                3 -> {
                    print("Input expression: ")
                    print("Result is: " + evaluateRPN(toRPN(readLine()!!)))
                }

                4 -> {
                    print("Input expression: ")
                    val rpn = toRPN(readLine()!!)
                    print("Reverse polish notation is: $rpn\n")
                    print ("Result is: " + evaluateRPN(rpn))
                }

                5 -> {
                    print("Finishing work.")
                    return
                }

                else -> {
                    print("Wrong input!")
                }
            }
        } catch (e: EmptyStackException) {
            print("Illegal brackets or operators disposition")
        } catch (e: IllegalBracketDispositionException) {
            print("Illegal brackets or operators disposition")
        }
        print("\n")
    }
}

/**
 * @param rpn reverse polish notation expression
 * @return value of expression
 */
fun evaluateRPN(rpn: String): Double {
    val values = defineVariables(rpn)
    val stack = Stack<Double>()
    val operands = rpn.splitExpression(' ')
    for (operand in operands) {
        if (operand.isOperator()) {
            val result: Double = if (operand == "~") {
                val firstStackValue = stack.pop()
                operand.applyOperator(firstStackValue, 0.0)
            } else {
                val firstStackValue = stack.pop()
                val secondStackValue = stack.pop()
                operand.applyOperator(secondStackValue, firstStackValue)
            }
            stack.push(result)
        } else {
            stack.push(values[operand])
        }
    }
    return stack.peek()
}

/**
 * Split string into variables, brackets and operators
 */
fun String.splitExpression(vararg delimiters: Char): List<String> {
    val result = mutableListOf<String>()
    var lastIndex = 0
    for (index in this.indices) {
        if (this[index] in delimiters) {
            if (lastIndex != index)
                result.add(this.substring(lastIndex, index))
            lastIndex = index + 1
        } else if (this[index].isOperatorOrBracket()) {
            if (lastIndex != index)
                result.add(this.substring(lastIndex, index)) // adding variable before iterator
            result.add(this[index].toString()) // adding operator
            lastIndex = index + 1
        }
    }
    val index = this.length
    if (lastIndex < index)
        result.add(this.substring(lastIndex, index))
    return result
}

/**
 * Defines variables in expression and requests for its values
 * @param expr expression
 * @return Map from name of variable to value
 */
fun defineVariables(expr: String): Map<String, Double> {
    val map = mutableMapOf<String, Double>()
    val operands = expr.splitExpression(' ')
    for (operand in operands) {
        if (!operand.isOperator()) {
            var value = operand.toDoubleOrNull()
            if (value == null)
                if (!map.containsKey(operand)) {
                    print("Variable $operand = ")
                    value = readLine()?.toDouble()!!
                } else {
                    // value = map[operand]
                    continue
                }
            map[operand] = value
        }
    }
    return map
}

/**
 * Convert expression to reverse polish notation
 * @param expr expression
 * @return reverse polish notation
 */
fun toRPN(expr: String): String {
    val stack = Stack<String>()
    var rpn = ""
    val operands = expr.splitExpression(' ')
    for ((index, operand) in operands.withIndex()) {
        when {
            operand == "(" -> stack.push(operand)

            operand == ")" -> {
                var sym = stack.pop()
                while (sym != "(") {
                    rpn += "$sym "
                    sym = stack.pop()
                }
            }

            operand == "-" || operand == "–" -> {
                if (index > 0 && (operands[index - 1] == ")" || !operands[index - 1].isOperatorOrBracket())) { // binary minus
                    while (!stack.empty() && getPriority(stack.peek()) >= getPriority(operand)) {
                        rpn += "${stack.pop()} "
                    }
                    stack.push(operand)
                } else { // unary minus
                    stack.push("~")
                }
            }

            operand.isOperator() -> {
                while (!stack.empty() && getPriority(stack.peek()) >= getPriority(operand)) {
                    rpn += "${stack.pop()} "
                }
                stack.push(operand)
            }

            else -> { // variable or digit
                rpn += "$operand "
            }
        }
    }
    while (!stack.isEmpty()) {
        val topElement = stack.pop()
        if (!topElement.isOperator())
            throw IllegalBracketDispositionException("")
        rpn += "$topElement "
    }
    return rpn
}

class IllegalBracketDispositionException(message: String) : Throwable(message = message)

/**
 * Check if character is operator
 */
fun Char.isOperator() =
    when (this) {
        '+', '-', '–', '~', '*', '/', '^' -> true // different minuses
        else -> false
    }

/**
 * Check if character is operator or bracket
 */
fun Char.isOperatorOrBracket() =
    if (this.isOperator())
        true
    else when (this) {
        '(', ')' -> true
        else -> false
    }

/**
 * Check if string is operator
 */
fun String.isOperator() =
    when (this) {
        "+", "-", "–", "~", "*", "/", "^" -> true // different minuses
        else -> false
    }

/**
 * Check if string is operator or bracket
 */
fun String.isOperatorOrBracket() =
    if (this.isOperator())
        true
    else when (this) {
        "(", ")" -> true
        else -> false
    }

/**
 * Apply operator to input values
 * @param lhs left hand side value
 * @param rhs right hand side value
 * @return result of an operation
 */
fun String.applyOperator(lhs: Double, rhs: Double): Double {
    return when (this) {
        "+" -> lhs + rhs
        "-", "–", -> lhs - rhs // different minuses
        "*" -> lhs * rhs
        "/" -> lhs / rhs
        "^" -> lhs.pow(rhs)
        "~" -> -lhs
        else -> throw IllegalAccessException("$this is not operator")
    }
}

/**
 * @return priority of operation
 */
fun getPriority(operation: Char) =
    when (operation) {
        '+', '-', '–', '~' -> 0 // different minuses
        '*', '/' -> 1
        '^' -> 2
        else -> -1
    }

/**
 * @return priority of operation
 */
fun getPriority(operation: String) =
    when (operation) {
        "+", "-", "–", "~" -> 0 // different minuses
        "*", "/" -> 1
        "^" -> 2
        else -> -1
    }